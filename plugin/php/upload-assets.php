<?php
header("Access-Control-Allow-Origin: *");
$target_directory = "/assets/";
if($_FILES)
{
$resultArray = array();
  foreach ( $_FILES as $file)
  {
    $fileName = $file['name'];
    $tmpName = $file['tmp_name'];
    $fileSize = $file['size'];
    $fileType = $file['type'];
    $fileTypeFamily = explode('/', $file['type'])[0];
    
    if ($file['error'] != UPLOAD_ERR_OK)
    {
      error_log($file['error']);
      echo JSON_encode(null);
    }
    $fp = fopen($tmpName, 'r');
    $content = fread($fp, filesize($tmpName));
    fclose($fp);
    $filename = $fileName;
    $uploaded = move_uploaded_file($tmpName, './..'.$target_directory.$fileTypeFamily.'/'.$fileName);
    
    
    $allowed_file_type = [
    	'image' => ['image/jpeg', 'image/png'],
    	'video' => ['video/mp4']
    ];
    
    if(!in_array($fileType, $allowed_file_type[$fileTypeFamily])){
    	echo json_encode([
    		'message' => 'Upload Error'	
    	]);	
    }
    
    $result=array(
		  'name'=> $file['name'],
		  'type'=> $fileTypeFamily,
		  'src'=> "https://" . $_SERVER['SERVER_NAME'].$target_directory.$fileTypeFamily.'/'.$fileName,
		  'height'=> 350,
		  'width'=> 250
    );

    array_push($resultArray,$result);
  }
$response = array( 'data' => $resultArray );
echo json_encode($response);
}
?>