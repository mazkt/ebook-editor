(function(){
	 document.addEventListener('DOMContentLoaded', function () {
		new Zooming({
			bgColor: 'rgb(0, 0, 0)',
			bgOpacity: 0.8,
			customSize: '90%'
		}).listen('.img-zoom')
	  })
})();

(function(){
	$('[data-paver]').paver();
})();


// (function(){
// 	$(document).ready(function(){
//
// 		const siemas = document.querySelectorAll('.siema');
//
// 		Siema.prototype.addArrows = function() {
//
// 		  this.prevArrow = document.createElement('button');
// 		  this.nextArrow = document.createElement('button');
// 		  this.prevArrow.textContent = '<';
// 		  this.nextArrow.textContent = '>';
// 		  this.selector.appendChild(this.prevArrow)
// 		  this.selector.appendChild(this.nextArrow)
//
// 		  this.prevArrow.addEventListener('click', () => this.prev());
// 		  this.nextArrow.addEventListener('click', () => this.next());
//
// 		}
//
// 		for(const siema of siemas) {
// 		   const instance = new Siema({
// 			 selector: siema,
// 			 perPage: 1,
// 			 loop: true,
// 			 duration: 200,
// 			 easing: 'cubic-bezier(.17,.67,.32,1.34)',
// 		  });
// 		   instance.addArrows();
// 		}
//
// 	});
// })();
