import Vue from 'vue'
import grapesjs from 'grapesjs'
import $ from 'jquery'
require('grapesjs-plugin-export')

Vue.mixin({
    methods: {
        gramateria() {
            var seoTitle = (localStorage.getItem('seo_title'))?localStorage.getItem('seo_title'):"Ibook Customization";
            var seoAuthor = (localStorage.getItem('seo_author'))?localStorage.getItem('seo_author'):"Ibook";
            var seoDesc = (localStorage.getItem('seo_desc'))?localStorage.getItem('seo_desc'):"Ibook - web builder.";
            var seoKey = (localStorage.getItem('seo_keywords'))?localStorage.getItem('seo_keywords'):"HTML5,CSS5,Generator,Web,Maker,Builder,GrapeJS";
            var chapter = localStorage.getItem('project_chapter');
            var title = localStorage.getItem('project_title');
            var editor = grapesjs.init({
                allowScripts: 1,
                showOffsets: 1,
                avoidInlineStyle: true,
                autorender: 0,
                noticeOnUnload: 0,
                container: '#gjs',
                height: '100%',
                fromElement: true,
                clearOnRender: 0,
                plugins: [
                  'gjs-plugin-export'
                ],
                pluginsOpts: {
                    'gjs-plugin-export': {
                        btnLabel: 'Upload Zip',
                        preHtml: `<!doctype html>
                        <html>
                        <head>
                        <title>${seoTitle}</title>
                        <!-- Required meta tags -->
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <meta name="description" content="${seoDesc}">
                        <meta name="keywords" content="${seoKey}">
                        <meta name="author" content="${seoAuthor}">
                        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
                        <link rel="stylesheet" href="https://editor.lambochat.tk/css/global.css">
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paver/1.3.4/css/paver.css">
                        <link rel="stylesheet" href="css/${chapter}-${title}.css">
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/zooming/2.1.0/zooming.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/paver/1.3.4/js/jquery.paver.min.js"></script>
                        <script src="https://editor.lambochat.tk/js/main.js"></script>
                        </head>
                        <body>
                        <div class='container'>`,
                        postHtml: `
                        </div>
                        </body>
                        </html>`
                    }
                },
                canvas: {
                    styles: ['https://fonts.googleapis.com/css?family=Roboto', 'https://fonts.googleapis.com/icon?family=Material+Icons', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', ,'https://editor.lambochat.tk/css/overide.css', 'https://editor.lambochat.tk/css/global.css'],
                    scripts: ['https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/paver/1.3.4/js/jquery.paver.min.js', 'https://editor.lambochat.tk/js/main.js']
                },
                commands: {
                    defaults: [
                        {
                            id: 'undo',
                            run: function (editor, sender) {
                                sender.set('active', false);
                                editor.UndoManager.undo(true);
                            }
                        }, {
                            id: 'redo',
                            run: function (editor, sender) {
                                sender.set('active', false);
                                editor.UndoManager.redo(true);
                            }
                        }, {
                            id: 'clean-all',
                            run: function (editor, sender) {
                                sender.set('active', false);
                                if (confirm('Are you sure to clean the canvas?')) {
                                    editor.runCommand('core:canvas-clear');
                                }
                            }
                        }],
                },
                assetManager: {
                    storageType : '',
                    storeOnChange : true,
                    storeAfterUpload : true,
                    upload: 'https://cdn.wznshdzn.name/assets/upload',
                    embedAsBase64: false,
                    uploadText: 'Drop files here or click to upload <br/> (Max File Size: 4MB)',
                    assets: [],
                    uploadFile: function(e){
                      var files = e.dataTransfer ? e.dataTransfer.files : e.target.files;
                      var formData = new FormData();
                      for(var i in files){
                        formData.append('file-'+i, files[i]) //containing all the selected images from local
                      }
                      $.ajax({
                        url: 'https://editor.lambochat.tk/php/upload-assets.php',
                        type: 'POST',
                        data: formData,
                        contentType:false,
                        crossDomain: true,
                        dataType: 'json',
                        mimeType: 'multipart/form-data',
                        processData:false,
                        xhrFields: {
                          withCredentials: false
                        },
                        header: {
                          accept: 'application/json',
                          'Access-Control-Allow-Origin': '*'
                        },
                        success: function(result){
                          var myJSON = [];
                          $.each( result['data'], function( key, value ) {
                            myJSON[key] = value;
                          });
                          var images = myJSON;
                          editor.AssetManager.add(images); //adding images to asset manager of GrapesJS
                          console.log(result);
                        }
                      });
                    },
                },
                blockManager: {
                    blocks: [
                        // Grid
                        {
                            id: 'b1',
                            label: '1 Block',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-reorder'
                            },
                            content:
                             '<div class=\'page-content\'>'
                            +  '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-full" data-gjs-draggable=".row" data-gjs-custom-name="col12"></div>'
                            +  '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            +'</div>'
                            // ``
                        },
                        {
                            id: 'b2',
                            label: '2 Blocks',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-th-large'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class=\'row\' data-gjs-droppable=\'.cell\' data-gjs-custom-name=\'Row\'>'
                            +      '<div class=\'col-6\' data-gjs-draggable=\'.row\' data-gjs-custom-name=\'m6\'></div>'
                            +      '<div class=\'col-6\' data-gjs-draggable=\'.row\' data-gjs-custom-name=\'m6\'></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>'
                        },
                        {
                            id: 'b3',
                            label: '3 Blocks',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-th'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-4" data-gjs-draggable=".row" data-gjs-custom-name="m4"></div>'
                            +     '<div class="col-4" data-gjs-draggable=".row" data-gjs-custom-name="m4"></div>'
                            +     '<div class="col-4" data-gjs-draggable=".row" data-gjs-custom-name="m4"></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>',
                        },
                        {
                            id: 'b4',
                            label: '4 Blocks',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-calendar'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-3" data-gjs-draggable=".row" data-gjs-custom-name="m3"></div>'
                            +     '<div class="col-3" data-gjs-draggable=".row" data-gjs-custom-name="m3"></div>'
                            +     '<div class="col-3" data-gjs-draggable=".row" data-gjs-custom-name="m3"></div>'
                            +     '<div class="col-3" data-gjs-draggable=".row" data-gjs-custom-name="m3"></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>',
                        },
                        {
                            id: 'b5',
                            label: '4/8 Block',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-th-list'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-4" data-gjs-draggable=".row" data-gjs-custom-name="m4"></div>'
                            +     '<div class="col-8" data-gjs-draggable=".row" data-gjs-custom-name="m8"></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>',
                        },
                        {
                            id: 'b6',
                            label: '5/7 Block',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-dedent'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-5" data-gjs-draggable=".row" data-gjs-custom-name="m5"></div>'
                            +     '<div class="col-7" data-gjs-draggable=".row" data-gjs-custom-name="m7"></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>',
                        },
                        {
                            id: 'b7',
                            label: '3/9 Block',
                            category: 'Grid',
                            attributes: {
                                class: 'fa fa-list'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="col-3" data-gjs-draggable=".row" data-gjs-custom-name="m3"></div>'
                            +     '<div class="col-9" data-gjs-draggable=".row" data-gjs-custom-name="m9"></div>'
                            +   '</div><style>[class^="col-"]:empty{display:table-cell;height:75px}</style>'
                            + '</div>',
                        },
                        {
                            id: 'b8',
                            label: 'Center Block',
                            category: 'Grid',
                            attributes: {
                                class: 'fa  fa-minus-square-o'
                            },
                            content:
                              '<div class=\'page-content\'>'
                            +   '<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">'
                            +     '<div class="center-box" data-gjs-draggable=".row"></div>'
                            +   '</div><style>.center-box:empty {height: 75px;}</style>'
                            + '</div>',
                        },
                        // Grid End

                        // Page
                        {
                            id: 'h2p',
                            label: 'Page Header With Image',
                            category: 'Page',
                            content:
                             '<div class=\'page-header-img\'>'
                            +   '<div class=\'page-subtitle\'><span>{{ Chapter No }}</span></div>'
                            +   '<div class=\'page-title\'><span>{{ Chapter Title }}</span></div>'
                            +   '<div class=\'page-decoration\'>'
                            +       '<img src="https://via.placeholder.com/150x100/0174DF/ffffff/" class="img-full">'
                            +   '</div>'
                            +'</div>',
                            attributes: {
                                class: 'fa fa-align-center'
                            }
                        },
                        {
                            id: 'h3p',
                            label: 'Page Header',
                            category: 'Page',
                            content:
                             '<div class=\'page-header\'>'
                            +   '<div class=\'page-subtitle\'><span>{{ Chapter  No }}</span></div>'
                            +   '<div class=\'page-title\'><span>{{ Chapter  Title }}</span></div>'
                            +'</div>',
                            attributes: {
                                class: 'fa fa-align-center'
                            }
                        },
                        {
                            id: 'h4p',
                            label: 'Page Flavor',
                            category: 'Page',
                            content:
                             '<div class=\'page-header\'>'
                            +   '<div class=\'page-flavor\'><span>{{ Chapter Flavor }}</span></div>'
                            +'</div>',
                            attributes: {
                                class: 'fa fa-align-center'
                            }
                        },
                        {
                            id: 'h5p',
                            label: 'Page Footer',
                            category: 'Page',
                            content: '<div class=\'page-footer\'><span>{{ Page No }}</span></div>',
                            attributes: {
                                class: 'fa fa-align-center'
                            }
                        },
                        {
                          id: 'h6p',
                          label: 'Image Caption',
                          category: 'Page',
                          content: '<div class=\'image-caption\'><span>- {{ Image Caption }} -</span></div>',
                          attributes: {
                              class: 'fa fa-align-center'
                          }
                        },
                        {
                            id: 'quo 2',
                            label: 'Blockquote Block',
                            category: 'Page',
                            content: '<blockquote class=\'block\'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</blockquote>',
                            attributes: {
                                class: 'fa fa-quote-right'
                            }
                        },
                        {
                            id: 'quo 3',
                            label: 'Blockquote Quote',
                            category: 'Page',
                            content: '<blockquote class=\'quote\'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</blockquote>',
                            attributes: {
                                class: 'fa fa-quote-right'
                            }
                        },
                        // Page End

                        // Paragraph
                        {
                            id: 'text',
                            label: 'Text',
                            attributes: {
                                class: 'fa fa-text-width'
                            },
                            category: 'Paragraph',
                            content: '<p>Insert your text here</p>'
                        },
                        {
                            id: 'text2',
                            label: 'Text Drop Cap',
                            attributes: {
                                class: 'fa fa-text-width'
                            },
                            category: 'Paragraph',
                            content: '<p class=\'dropcap\'>Insert your text here</p>'
                        },
                        {
                            id: 'link',
                            label: 'Link',
                            category: 'Paragraph',
                            attributes: {
                                class: 'fa fa-link'
                            },
                            content: {
                                type: 'link',
                                content: 'Link',
                            },
                        },
                        // Paragraph End

                        // Images
                        {
                            id: 'image',
                            label: 'Image Non Responsive',
                            category: 'Images',
                            attributes: {
                                class: 'fa fa-file-image-o'
                            },
                            content: {
                                type: 'image',
                                activeOnRender: 1
                            },
                        },
                        {
                            id: 'res',
                            label: 'Responsive & Zoom Image Full',
                            category: 'Images',
                            attributes: {
                                class: 'fa fa-image'
                            },
                            content:
                              '<div class=\'page-image-full\'>'
                            +   '<img class="img-zoom img-full" src="https://via.placeholder.com/600x200/0174DF/ffffff/">'
                            + '</div>'
                        },
                        {
                            id: 'zoom',
                            label: 'Responsive & Zoom Image Potrait',
                            category: 'Images',
                            attributes: {
                                class: 'fa fa-object-group'
                            },
                            content:
                              '<div class=\'page-image-potrait\'>'
                            +   '<img class="img-zoom img-full" src="https://via.placeholder.com/250x250/BF00FF/ffffff/">'
                            + '</div>'
                        },
                        // Images End

                        // List
                        {
                            id: 'listUl',
                            label: 'Unordered ',
                            category: 'List',
                            attributes: {
                                class: 'fa fa-list-ul'
                            },
                            content: `<ul class="unordered-list">
                            <li>List One</li>
                            <li>List Two</li>
                            <li>List Three</li>
                            <li>List Four</li>
                            </ul>`,
                        },
                        {
                            id: 'listOl',
                            label: 'Ordered',
                            category: 'List',
                            attributes: {
                                class: 'fa fa-list-ol'
                            },
                            content: `<ol class="ordered-list">
                            <li>List One</li>
                            <li>List Two</li>
                            <li>List Three</li>
                            <li>List Four</li>
                            </ul>`,
                        },
                        {
                            id: 'listUl2',
                            label: 'Unordered Square',
                            category: 'List',
                            attributes: {
                                class: 'fa fa-list'
                            },
                            content: `<ul class="unordered-list square">
                            <li>List One</li>
                            <li>List Two</li>
                            <li>List Three</li>
                            <li>List Four</li>
                            </ul>`,
                        },
                        {
                            id: 'listUl3',
                            label: 'Unordered None',
                            category: 'List',
                            attributes: {
                                class: 'fa fa-bars'
                            },
                            content: `<ul class="unordered-list none">
                            <li>List One</li>
                            <li>List Two</li>
                            <li>List Three</li>
                            <li>List Four</li>
                            </ul>`,
                        },
                        // List End

                        // Table of Content
                        {
                            id: 'toc 1',
                            label: 'Toc Header',
                            category: 'Table Of Contents',
                            attributes: {
                                class: 'fa  fa-list-alt'
                            },
                            content:
                              '<div class=\'toc-header\'>'
                            +   '<span>{{ Toc Header }}</span>'
                            + '</div>'
                        },
                        {
                            id: 'toc 2',
                            label: 'Toc Header with Pages No.',
                            category: 'Table Of Contents',
                            attributes: {
                              class: 'fa fa-list-alt'
                            },
                            content:
                              '<div class=\'toc-header-no\'>'
                            +   '<span>{{ Toc Header }}</span>'
                            +   '<span>{00}</span>'
                            + '</div>'
                        },
                        {
                            id: 'toc 3',
                            label: 'Toc Content',
                            category: 'Table Of Contents',
                            attributes: {
                                class: 'fa  fa-list-alt'
                            },
                            content:
                              '<div class=\'toc-content\'>'
                            +   '<span>{{ Toc Content }}</span>'
                            + '</div>'
                        },
                        {
                            id: 'toc 4',
                            label: 'Toc Content with Pages No.',
                            category: 'Table Of Contents',
                            attributes: {
                              class: 'fa fa-list-alt'
                            },
                            content:
                              '<div class=\'toc-content-no\'>'
                            +   '<span>{{ Toc Content }}</span>'
                            +   '<span>{00}</span>'
                            + '</div>'
                        },
                        // Table of Content End

                        // Table
                        {
                            id: 'table1',
                            label: 'Table Base',
                            category: 'Table',
                            attributes: {
                              class: 'fa fa-table'
                            },
                            content: {
                              type: 'table'
                            }

                        },
                        {
                          id: 'table3',
                          label: 'Table Cell',
                          category: 'Table',
                          attributes: {
                            class: 'fa fa-table'
                          },
                          content: {
                            type: 'cell'
                          }
                        },
                        // Table End

                        // Typography
                        {
                            id: 'h1p',
                            label: 'Text section',
                            category: 'Typography',
                            content: '<div>'
                            + '<h3>Insert title here</h3>'
                            + '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>'
                            + '</div>',
                            attributes: {
                                class: 'fa fa-align-justify'
                            }
                        },
                        {
                            id: 'hr1',
                            label: 'Horizontal lines 01',
                            category: 'Typography',
                            attributes: {
                                class: 'fa fa-font'
                            },
                            content: '<hr class=\'hr-line-01\'/>'
                        },
                        {
                            id: 'hr2',
                            label: 'Horizontal lines 02',
                            category: 'Typography',
                            attributes: {
                                class: 'fa fa-font'
                            },
                            content: '<hr class=\'hr-line-02\'/>'
                        },
                        {
                            id: 'br',
                            label: 'Break lines',
                            category: 'Typography',
                            attributes: {
                                class: 'fa fa-font'
                            },
                            content: '<br />'
                        },
                        // Typography End

                        // Video
                        {
                            id: 'videoCont',
                            label: 'Video Container',
                            category: 'Media',
                            attributes: {
                                class: 'fa fa-youtube-play'
                            },
                            content:
                              '<div class =\'page-video\'>'
                            + '</div>'
                            + '<style>'
                            +   '.page-video:empty{ height: 75px; }'
                            + '</style>'

                        },
                        {
                            id: 'video',
                            label: 'Video',
                            category: 'Media',
                            attributes: {
                                class: 'fa fa-youtube-play'
                            },
                            content: {
                                type: 'video',
                                src: ''
                            }
                        },
                        // Video End

                        // Panorama
                        {
                            id: 'panorama',
                            label: 'Panorama',
                            category: 'Panorama',
                            attributes: {
                                class: 'fa fa-youtube-play'
                            },
                            content:
                              '<div class=\'page-panorama\' data-pager>'
                            +   '<img src="https://via.placeholder.com/1200x400/0174DF/ffffff/" />'
                            + '</div>'
                        }
                        // Panorama End


                    ],
                },
                storageManager: {
                    id: 'gjs-',
                    type: 'local',
                    autosave: 1,
                    autoload: 1,
                    stepsBeforeSave: 1,
                    storeComponents: 1,
                    storeStyles: 0,
                    storeHtml: 1,
                    storeCss: 1,
                },
                styleManager: {
                    sectors: [
                    {
                        name: 'General',
                        open: false,
                        buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom']
                    },
                    {
                        name: 'Dimension',
                        open: false,
                        buildProps: ['width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
                    },
                    {
                        name: 'Typography',
                        open: false,
                        buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-shadow'],
                        properties: [
                            {
                                property: 'text-align',
                                list: [{
                                        value: 'left',
                                        className: 'fa fa-align-left'
                                    },
                                    {
                                        value: 'center',
                                        className: 'fa fa-align-center'
                                    },
                                    {
                                        value: 'right',
                                        className: 'fa fa-align-right'
                                    },
                                    {
                                        value: 'justify',
                                        className: 'fa fa-align-justify'
                                    }
                                ],
                            }
                      ]
                    },
                    {
                        name: 'Decorations',
                        open: false,
                        buildProps: ['border-radius-c', 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
                    },
                    {
                        name: 'Extra',
                        open: false,
                        buildProps: ['opacity', 'transition', 'perspective', 'transform'],
                        properties: [{
                            type: 'slider',
                            property: 'opacity',
                            defaults: 1,
                            step: 0.01,
                            max: 1,
                            min: 0,
                        }]
                    },
                    {
                        name: 'Flex',
                        open: false,
                        properties: [{
                            name: 'Flex Container',
                            property: 'display',
                            type: 'select',
                            defaults: 'block',
                            list: [{
                                value: 'block',
                                name: 'Disable',
                            }, {
                                value: 'flex',
                                name: 'Enable',
                            }
                        ],
                    },
                    {
                            name: 'Flex Parent',
                            property: 'label-parent-flex',
                        },
                        {
                            name: 'Direction',
                            property: 'flex-direction',
                            type: 'radio',
                            defaults: 'row',
                            list: [{
                                value: 'row',
                                name: 'Row',
                                className: 'icons-flex icon-dir-row',
                                title: 'Row',
                            },
                            {
                                value: 'row-reverse',
                                name: 'Row reverse',
                                className: 'icons-flex icon-dir-row-rev',
                                title: 'Row reverse',
                            },
                            {
                                value: 'column',
                                name: 'Column',
                                title: 'Column',
                                className: 'icons-flex icon-dir-col',
                            },
                            {
                                value: 'column-reverse',
                                name: 'Column reverse',
                                title: 'Column reverse',
                                className: 'icons-flex icon-dir-col-rev',
                            }],
                        },
                        {
                            name: 'Wrap',
                            property: 'flex-wrap',
                            type: 'radio',
                            defaults: 'nowrap',
                            list: [{
                                value: 'nowrap',
                                title: 'Single line',
                            }, {
                                value: 'wrap',
                                title: 'Multiple lines',
                            }, {
                                value: 'wrap-reverse',
                                title: 'Multiple lines reverse',
                            }],
                        },
                        {
                            name: 'Justify',
                            property: 'justify-content',
                            type: 'radio',
                            defaults: 'flex-start',
                            list: [{
                                value: 'flex-start',
                                className: 'icons-flex icon-just-start',
                                title: 'Start',
                            }, {
                                value: 'flex-end',
                                title: 'End',
                                className: 'icons-flex icon-just-end',
                            }, {
                                value: 'space-between',
                                title: 'Space between',
                                className: 'icons-flex icon-just-sp-bet',
                            }, {
                                value: 'space-around',
                                title: 'Space around',
                                className: 'icons-flex icon-just-sp-ar',
                            }, {
                                value: 'center',
                                title: 'Center',
                                className: 'icons-flex icon-just-sp-cent',
                            }],
                        },
                        {
                            name: 'Align',
                            property: 'align-items',
                            type: 'radio',
                            defaults: 'center',
                            list: [{
                                value: 'flex-start',
                                title: 'Start',
                                className: 'icons-flex icon-al-start',
                            }, {
                                value: 'flex-end',
                                title: 'End',
                                className: 'icons-flex icon-al-end',
                            }, {
                                value: 'stretch',
                                title: 'Stretch',
                                className: 'icons-flex icon-al-str',
                            }, {
                                value: 'center',
                                title: 'Center',
                                className: 'icons-flex icon-al-center',
                            }],
                        },
                        {
                            name: 'Flex Children',
                            property: 'label-parent-flex',
                        },
                        {
                            name: 'Order',
                            property: 'order',
                            type: 'integer',
                            defaults: 0,
                            min: 0
                        },
                        {
                            name: 'Flex',
                            property: 'flex',
                            type: 'composite',
                            properties: [{
                                name: 'Grow',
                                property: 'flex-grow',
                                type: 'integer',
                                defaults: 0,
                                min: 0
                            },
                            {
                                name: 'Shrink',
                                property: 'flex-shrink',
                                type: 'integer',
                                defaults: 0,
                                min: 0
                            },
                            {
                                name: 'Basis',
                                property: 'flex-basis',
                                type: 'integer',
                                units: ['px', '%', ''],
                                unit: '',
                                defaults: 'auto',
                            }],
                        },
                        {
                            name: 'Align',
                            property: 'align-self',
                            type: 'radio',
                            defaults: 'auto',
                            list: [{
                                value: 'auto',
                                name: 'Auto',
                            }, {
                                value: 'flex-start',
                                title: 'Start',
                                className: 'icons-flex icon-al-start',
                            }, {
                                value: 'flex-end',
                                title: 'End',
                                className: 'icons-flex icon-al-end',
                            }, {
                                value: 'stretch',
                                title: 'Stretch',
                                className: 'icons-flex icon-al-str',
                            }, {
                                value: 'center',
                                title: 'Center',
                                className: 'icons-flex icon-al-center',
                            }],
                        }]
                    }

                    ],

                },
            });

            var pnm = editor.Panels;
            pnm.addButton('options', [
                {
                    id: 'undo',
                    className: 'fa fa-undo icon-undo',
                    command: function (editor, sender) {
                        sender.set('active', 0);
                        editor.UndoManager.undo(1);
                    },
                    attributes: {
                        title: 'Undo (CTRL/CMD + Z)'
                    }
                },
                {
                    id: 'redo',
                    className: 'fa fa-repeat icon-redo',
                    command: function (editor, sender) {
                        sender.set('active', 0);
                        editor.UndoManager.redo(1);
                    },
                    attributes: {
                        title: 'Redo (CTRL/CMD + SHIFT + Z)'
                    }
                },
                {
                    id: 'import',
                    className: 'fa fa-edit',
                    command: 'html-edit',
                    attributes: {
                        title: 'Import'
                    }
                },
                {
                    id: 'clean-all',
                    className: 'fa fa-trash icon-blank',
                    command: function (editor, sender) {
                        if (sender) sender.set('active', false);
                        if (confirm('Are you sure to clean the canvas?')) {
                            editor.runCommand('core:canvas-clear');
                            setTimeout(function () {
                                localStorage.setItem('gjs-assets', '');
                                localStorage.setItem('gjs-components', '');
                                localStorage.setItem('gjs-html', '');
                                localStorage.setItem('gjs-css', '');
                                localStorage.setItem('gjs-styles', '');
                            }, 0);
                        }
                    },
                    attributes: {
                        title: 'Empty canvas'
                    }
                }
            ]);

// ---------------------
// Import/Edit
// ---------------------
            var gra = {
                // append in container
                _a: function (appendName) {
                    return container.appendChild(appendName);
                },
                // create elements
                _c: function (tagName) {
                    return document.createElement(tagName);
                },
                // check extensions
                _e: function (fname) {
                    var ext = /^.+\.([^.]+)$/.exec(fname);
                    return ext == null ? "" : ext[1];
                },
                // select id
                _d: function (name, tag) {
                    switch (tag) {
                        case 'class':
                            return document.getElementsByClassName(name);
                        case 'id':
                            return document.getElementById(name);
                        default:
                            return document.getElementById(name);
                    }
                }
            }
            var pfx = editor.getConfig().stylePrefix;
            var modal = editor.Modal;
            var cmdm = editor.Commands;
            var container = gra._c("div");
            var btnEdit = gra._c("button");
            var copyHtml = gra._c("button");
            var copyCss = gra._c("button");
            var btnZip = gra._c("button");
            var exportTxt = gra._c("button");
            var loadTxt = gra._c("button");
            var fileLoader = gra._c("form");
            var anchor = gra._c("a");


            function buildCodeEditor(type) {
                var codeEditor = editor.CodeManager.getViewer('CodeMirror').clone();
                codeEditor.set({
                    codeName: type === 'html' ? 'htmlmixed' : 'css',
                    readOnly: 0,
                    theme: 'hopscotch',
                    autoBeautify: true,
                    autoCloseTags: true,
                    autoCloseBrackets: true,
                    lineWrapping: true,
                    styleActiveLine: true,
                    smartIndent: true,
                    indentWithTabs: true
                });
                return codeEditor;
            }
            var htmlCodeEditor = buildCodeEditor('html');
            var cssCodeEditor = buildCodeEditor('css');

            btnEdit.innerHTML = '<i class="fa fa-code"></i> Apply';
            btnZip.setAttribute('name', 'file');
            btnZip.innerHTML = '<i class="fa fa-download"></i> Upload Project (zip)';
            exportTxt.innerHTML = '<i class="fa fa-download"></i> Save as .gram file';
            loadTxt.innerHTML = '<i class="fa fa-upload"></i> Load .gram file';
            copyHtml.innerHTML = '<i class="fa fa-copy"></i> Copy HTML';
            copyCss.innerHTML = '<i class="fa fa-copy"></i> Copy CSS';
            fileLoader.innerHTML = '<input type="file" id="fileToLoad">';

            fileLoader.className = pfx + 'import-file';
            btnEdit.className = pfx + 'btn-prim ' + pfx + 'btn-import';
            copyHtml.className = pfx + 'btn-prim ' + pfx + 'btn-html';
            copyCss.className = pfx + 'btn-prim ' + pfx + 'btn-css';
            btnZip.className = pfx + 'btn-prim ' + pfx + 'btn-zip';
            exportTxt.className = pfx + 'btn-prim ' + pfx + 'btn-export';
            loadTxt.className = pfx + 'btn-prim ' + pfx + 'btn-load';

// import button inside import editor
            btnEdit.onclick = function () {
                var htmlCode = htmlCodeEditor.editor.getValue();
                var cssCode = cssCodeEditor.editor.getValue();
                editor.DomComponents.getWrapper().set('content', '');
                editor.setComponents(htmlCode.trim() + '<style>' + cssCode.trim() + '</style>');
                modal.close();
            };

            btnZip.onclick = function() {
                editor.runCommand('gjs-export-zip');
            };

// onclick load file button inside import editor
            loadTxt.onclick = function (e) {
                e.preventDefault();
                var fileToLoad = document.getElementById("fileToLoad").files[0];
                var fType = gra._e(fileToLoad['name']);
                if (fileToLoad === undefined) {
                    alert('Please select a file');
                    return;
                }
                if (fType === 'gram' || fType === 'txt') {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var fileData = e.target.result;
                        editor.DomComponents.getWrapper().set('content', '');
                        editor.setComponents(fileData);
                        modal.close();
                    }
                    reader.readAsText(fileToLoad);
                } else {
                    alert('You can only import .gram or .txt extension');
                }
            }

            copyHtml.onclick = function(){
                var htmlCodes = htmlCodeEditor.editor.getValue();
                var dummy = gra._c("input");
                document.body.appendChild(dummy);
                dummy.setAttribute('value', htmlCodes);
                dummy.select();
                document.execCommand("copy");
                document.body.removeChild(dummy);
                document.execCommand('copy');
                alert('You have copied HTML codes!');
            };
            copyCss.onclick = function(){
                var cssCodes = cssCodeEditor.editor.getValue();
                var dummy = gra._c("input");
                document.body.appendChild(dummy);
                dummy.setAttribute('value', cssCodes);
                dummy.select();
                document.execCommand("copy");
                document.body.removeChild(dummy);
                document.execCommand('copy');
                alert('You have copied CSS codes!');
            };

// onclick save as button inside import editor
            exportTxt.onclick = function () {
                    var InnerHtml = editor.getHtml();
                    var Css = editor.getCss();
                    var text = InnerHtml + "<style>" + Css + '</style>';
                    var blob = new Blob([text], {
                        type: "text/plain"
                    });
                    anchor.download = "download.gram";
                    anchor.href = window.URL.createObjectURL(blob);
                    anchor.target = "_blank";
                    anchor.style.display = "none"; // just to be safe!
                    document.body.appendChild(anchor);
                    anchor.click();
                    document.body.removeChild(anchor);
            }

// import nav button click event
            cmdm.add('html-edit', {
                run: function importArea(editor, sender) {
                    sender && sender.set('active', 0);
                    var htmlViewer = htmlCodeEditor.editor;
                    var cssViewer = cssCodeEditor.editor;
                    var htmlBox = gra._c('div');
                    htmlBox.className = 'html-wrapper';
                    htmlBox.innerHTML = "<h4>HTML</h4>";
                    var cssBox = gra._c('div');
                    cssBox.className = 'css-wrapper';
                    cssBox.innerHTML = "<h4>CSS</h4>";
                    modal.setTitle('Edit and Import');
                    var headline = gra._c('div');
                    headline.className = 'clear-head';
                    var htmlWrap = gra._c('textarea');
                    var cssWrap = gra._c('textarea');
                    htmlBox.appendChild(htmlWrap);
                    cssBox.appendChild(cssWrap);
                    if (!htmlViewer && !cssViewer) {
                        gra._a(fileLoader);
                        gra._a(loadTxt);
                        gra._a(exportTxt);
                        gra._a(headline);
                        gra._a(htmlBox);
                        gra._a(cssBox);
                        gra._a(copyCss);
                        gra._a(copyHtml);
                        gra._a(btnEdit);
                        gra._a(btnZip);
                        htmlCodeEditor.init(htmlWrap);
                        cssCodeEditor.init(cssWrap);
                    }
                    modal.setContent('');
                    modal.setContent(container);
                    htmlCodeEditor.setContent(editor.getHtml());
                    cssCodeEditor.setContent(editor.getCss({ avoidProtected: true }));
                    modal.open();
                    htmlCodeEditor.editor.refresh();
                    cssCodeEditor.editor.refresh();
                }
            });

            editor.BlockManager.getCategories().each(function (ctg) {
                ctg.set('open', false);
            })
            editor.Panels.removeButton('options', 'export-template');
            editor.render();
        }
    }
})
