export const firebase = require('firebase/app');

require("firebase/auth");
require("firebase/database");

// Initialize Firebase
let config = {
  apiKey: "AIzaSyD9GMShRnLeTv2WKznO8jb3iCxm1uyRzJ8",
  authDomain: "ibook-reader.firebaseapp.com",
  databaseURL: "https://ibook-reader.firebaseio.com",
  projectId: "ibook-reader",
  storageBucket: "ibook-reader.appspot.com",
  messagingSenderId: "392812710181"
}
firebase.initializeApp(config);
export const fb = firebase;
export const auth = fb.auth();
export const db = fb.database()
export const user_db = db.ref('users');
export const template_db = db.ref('templates');
